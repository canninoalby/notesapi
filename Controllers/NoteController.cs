using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotesAPI.Data;
using NotesAPI.Model;

namespace NotesAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NoteController : ControllerBase
    {
        // GET: api/note
        private readonly  DataContext _context;
        public NoteController(DataContext context)
        {
            _context = context;
        }

        [HttpGet,Authorize]
        public async Task<IActionResult> Get()
        {
            // TODO: Implement logic to retrieve all notes
            var notes = await _context.Notes.ToListAsync();
            return Ok(notes);
        }

        // GET: api/note/{id}
        [HttpGet("{id}"),Authorize]
        public async Task<IActionResult> Get(int id)
        {
            // TODO: Implement logic to retrieve a specific note by id
            var note = await _context.Notes.FirstOrDefaultAsync(n => n.Id == id);
            return Ok(note);
        }

        // POST: api/note
        [HttpPost,Authorize]
        public async Task<IActionResult> Post([FromBody] Note note)
        {
            // TODO: Implement logic to create a new note
            note.CreatedAt = DateTime.Now;
            note.UpdatedAt = DateTime.Now;
            await _context.Notes.AddAsync(note);
            await _context.SaveChangesAsync();
            return Ok(await _context.Notes.ToListAsync());
        }

        // PUT: api/note/{id}
        [HttpPut("{id}"),Authorize]
        public async Task<IActionResult> Put(int id, [FromBody] Note note)
        {
            // TODO: Implement logic to update a specific note by id
            var noteToUpdate = await _context.Notes.FirstOrDefaultAsync(n => n.Id == id);
            if (noteToUpdate == null)
            {
                return NotFound();
            }
            noteToUpdate.Title = note.Title;
            noteToUpdate.Content = note.Content;
            noteToUpdate.UpdatedAt = DateTime.Now;
            await _context.SaveChangesAsync();

            return Ok(await _context.Notes.FirstOrDefaultAsync(n => n.Id == id));
        }

        // DELETE: api/note/{id}
        [HttpDelete("{id}"),Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            // TODO: Implement logic to delete a specific note by id
            var noteToDelete = await _context.Notes.FirstOrDefaultAsync(n => n.Id == id);
            if (noteToDelete == null)
            {
                return NotFound();
            }
            _context.Notes.Remove(noteToDelete);
            await _context.SaveChangesAsync();

            return Ok(await _context.Notes.ToListAsync());
        }
    }
}
